# Temporal_Graph

**Implementation of the proposal in the article "A generic modelling to capture the temporal evolution in graphs" by Landy Andriamampianina, Franck Ravat, Jiefu Song and Nathalie Vallés-Parlangeau**

This repository presents the guidelines to implement our proposal  in two datastores: Neo4j and OrientDB as done in our article. 

***Abstract of the paper***
This paper describes a new temporal graph modelling solution to organize and memorize temporal evolution of a business application's data.
Our model relies on the concepts of \textit{states} and \textit{instances} to describe the components of a graph.
Our modelling has the following advantages: (i) representing different evolution levels of a graph (graph topology, states and instances), (ii) memorizing in an optimal manner the evolution traces and consequently retrieve easily temporal information about a graph component. 
To validate the feasibility of our proposal, we implement our proposal in two data stores based on property graph model: Neo4j and OrientDB.

***Running example***
We consider the temporal evolution of a bike life cycle, the phase from its manufacturing to its service, which lasts 3 days. 
Specifically, a bike travels down the production line through different departments over a timespan of 3 days.
Each department is responsible for the assembly of a specific product on the bike (such as a handlebar or a wheel etc.), which is itself the result of the assembly of components upstream of the bike assembly. 
Therefore, the bike, its relationships and related entities evolve several times during a day (i.e. changes in their schema and/or in the value of their attributes).

***Data stores installation***
To install locally Neo4j and OrientDB, use the two following URL links: 
* OrientDB: https://orientdb.org/download
* Neo4j: https://neo4j.com/download-thanks/?edition=community&release=3.3.2&flavour=winzip&_ga=2.155390971.1933374495

***Dataset***
The dataset of the running example is in the directory "Dataset". It is composed of 11 files .csv.

***Conceptual implementation***
In the directory called "Conceptual representation", there are two files to present the conceptual representation of the running example:
the textual and graphical representation. 

***Implementation on Neo4j***
To insert data in Neo4j, find in the directory called "Neo4j_Implementation" the file "script_data_insertion_neo4j" which is a script written in Cypher (in-built querying language of Neo4j) to run in the Neo4j interface. 
You will find the result of the data insertion in the image called "result_data_insertion_neo4j". 

***Implementation on OrientDB***
To insert data in OrientDB, find in the directory called "OrientDB_Implementation" the file "script_data_insertion_orientDB" which is a script written in SQL to run in the OrientDB interface. 
You will find the result of the data insertion in the image called "result_data_insertion_orientDB". 

***Queries***
We have written some queries to get insights about the temporal aspects of the graph: 
(i) to view the current instances of the bike life cycle and 
(ii) to view the different changes that occurred on the bike.
To run these queries on Neo4j, find in the directory "Neo4j_Implementation" the files "query_1_neo4j" and "query_2_neo4j" which present the query script and the vizualization of the query result. 
To run these queries on OrientDB, find in the directory "OrientDB_Implementation" the files "query_1_neo4j" and "query_2_neo4j" which present the query script and the vizualization of the query result. 
